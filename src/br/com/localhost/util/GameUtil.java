package br.com.localhost.util;

import javax.swing.*;

/**
 * Created by rodrigo on 18/07/17.
 */
public class GameUtil {
    private static final String[] optionalPaneButtons = {"SIM", "NÃO"};

    public static boolean getAnswer(String message) {
        int resp = JOptionPane.showOptionDialog(null,
                message,
                "Confirme",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, optionalPaneButtons,
                optionalPaneButtons[0]);

        return resp == 0;

    }

    public static void showMessage(String message){
        JOptionPane.showMessageDialog(null,message);
    }

    public static String getInputValue(String message) {
        return JOptionPane.showInputDialog(null,
                message);
    }
}
