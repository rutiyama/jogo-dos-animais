package br.com.localhost.model;

import java.util.Optional;

/**
 * Created by rodrigo on 18/07/17.
 */
public class Animal {
    private Optional<Animal> next;
    private Optional<Animal> previous;
    private Long id;
    private String name;

    public Animal() {
        this.id = id;
    }

    public Animal(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public Optional<Animal> getNext() {
        return next;
    }

    public void setNext(Optional<Animal> next) {
        this.next = next;
    }

    public Optional<Animal> getPrevious() {
        return previous;
    }

    public void setPrevious(Optional<Animal> previous) {
        this.previous = previous;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        return id != null ? id.equals(animal.id) : animal.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
