package br.com.localhost.model;

import br.com.localhost.util.GameUtil;

import java.util.Optional;

/**
 * Created by rodrigo on 18/07/17.
 */
public class BaseModel {
    private Optional<Animal> animal;

    public BaseModel(){
        this.animal = Optional.empty();
    }

    protected Optional<Animal> getAnimal(){
        return this.animal;
    }

    protected void setAnimal(Optional<Animal> animal){
        this.animal = animal;
    }

    protected boolean getAnswer(String message) {
        return GameUtil.getAnswer(message);
    }

    protected void showMessage(String message) {
        GameUtil.showMessage(message);
    }

    protected String getInputValue(String message) {
        return GameUtil.getInputValue(message);
    }

}
