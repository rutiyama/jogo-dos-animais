package br.com.localhost.app;

import br.com.localhost.controller.Game;

import javax.swing.*;
import java.awt.*;

/**
 * Created by rodrigo on 18/07/17.
 */
public class GameApplication extends JFrame {

    public GameApplication(){
        setTitle("Jogo dos animais");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JButton btnStart = new JButton("Iniciar");

        btnStart.addActionListener(event -> Game.getInstance().startGame());

        JPanel panel = new JPanel();
        Label label = new Label("Pense em um animal");

        panel.setSize(100, 30);
        panel.add(label);
        panel.add(btnStart);

        getContentPane().add(panel, BorderLayout.CENTER);
        setSize(200,150);
        setVisible(true);
    }

    public static void main (String[] args){
        new GameApplication();
    }
}
