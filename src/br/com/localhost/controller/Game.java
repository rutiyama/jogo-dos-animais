package br.com.localhost.controller;

import br.com.localhost.model.Animal;
import br.com.localhost.model.BaseModel;

import java.util.Optional;

/**
 * Created by rodrigo on 18/07/17.
 */
public final class Game extends BaseModel {
    private static Long id = 1L;
    private static Game game;

    /*
     * Foi utilizado o design pattern singleton para que não perder os valores da primeira
     * sessão de perguntas
     */
    public static Game getInstance(){
        if (game == null)
            game = new Game();

        return game;
    }

    private Game(){
        super();
        getAnimal().orElseGet(() -> initFirstAnimal().get());
    }

    public void startGame() {
        Animal animal = getAnimal().get();
        boolean answerYes = getAnswer("O animal que você pensou " + animal.getName() + " ?");

        if(answerYes)
            askUser(animal.getNext());
        else
            askUser(animal.getPrevious());
    }

    private void askUser(Optional<Animal> animal) {
        boolean answerYes = getAnswer("O animal que você pensou é " + animal.get().getName() + "?");

        /* Caso o nome ou qualidade do animal o sistema acertou,
        *  verifica se existe o próximo nó (atributo do tipo Animal). Caso exista,
        *  pergunta sobre o animal/qualidade do próxmo nó e assim por diante, de forma recursiva até acertar.
        *  Em caso de finalizar a lista encadeada e não acertar, o nó acaba com o usuário informando o
        *  animal e a qualidade (método askNewAnimal()).
        *
        *  Se caso o usuário dizer que não é o animal/qualidade, é verificado se possui um nó para quando
        *  ser falso. Se possuir, pergunta sobre o animal/qualidade. Caso contrário, pede para o usuário informar
        *  qual o animal e a sua qualidade.
        *
        *  Para cada novo animal, é adicionado de forma encadeada (também seguindo a ideia de árvore binária) e
        *  percorrendo-o de forma recursiva.
        * */
        if(answerYes) {
            if(animal.get().getNext() != null)
                askUser(animal.get().getNext());
            else
                showMessage("Acertei de novo!");
        } else {
            if(animal.get().getPrevious() != null)
                askUser(animal.get().getPrevious());
            else
                askNewAnimal(animal);
        }
    }

    private void askNewAnimal(Optional<Animal> animal) {
        String newAnimalName    = getInputValue("Qual o animal que você pensou?");
        String newAnimalFeature = getInputValue("Um(a) "
                + newAnimalName
                + ",______ mas um(a) "
                + animal.get().getName()
                + " não.");

        ++this.id;

        addAnimalToNext(animal, newAnimalFeature, newAnimalName);
    }

    /*
     * Este método adiciona o animal como o próximo elemento do último animal (n+1).
     * Para o elemento n, o próximo animal em caso de resposta verdadeira será o elemento n+1. Já em
     * caso de resposta falsa, será o mesmo valor de n
     */

    private void addAnimalToNext(Optional<Animal> animal, String feature, String name ) {
        String nameAnimal = animal.get().getName();
        animal.get().setName(feature);
        animal.get().setNext(Optional.of(new Animal(name, ++id)));
        animal.get().setPrevious(Optional.of(new Animal(nameAnimal, ++id)));
    }

    /*
     * Quando for realizado a primeira instância do jogo, dois animais são adicionados por padrão:
     * o Tubarão e Macaco, que são os animais perguntados no inicio do jogo. Primeiro é o que vive na água,
     * e se a resposta for sim, é adicionado o Tubarão como próximo elemento da árvore.
     *
     * Caso não, pergunta se é o macaco.
     */

    private Optional<Animal> initFirstAnimal() {
        Animal animal = new Animal();
        animal.setId(id);
        animal.setName("Vive na água");
        animal.setNext(Optional.of(new Animal("Tubarão", ++id)));
        animal.setPrevious(Optional.of(new Animal("Macaco", ++id)));

        Optional<Animal> animalWrapped = Optional.of(animal);
        setAnimal(animalWrapped);
        return animalWrapped;
    }
}
